import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LecturersRoutingModule } from './lecturers-routing.module';
import { LecturersComponent } from './lecturers.component';
import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [CommonModule, LecturersRoutingModule, PageHeaderModule],
    declarations: [LecturersComponent]
})
export class LecturersModule {}
