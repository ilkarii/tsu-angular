import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LecturersComponent } from './lecturers.component';

const routes: Routes = [
    {
        path: '', component: LecturersComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LecturersRoutingModule {
}
