import { async, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'

import { LecturersComponent } from './lecturers.component'
import { LecturersModule } from './lecturers.module'

describe('LecturersComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ LecturersModule, RouterTestingModule ],
    })
    .compileComponents()
  }))

  it('should create', () => {
    const fixture = TestBed.createComponent(LecturersComponent)
    const component = fixture.componentInstance
    expect(component).toBeTruthy()
  })
})
