import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-lecturers',
    templateUrl: './lecturers.component.html',
    styleUrls: ['./lecturers.component.scss'],
    animations: [routerTransition()]
})
export class LecturersComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
