import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-subjects',
    templateUrl: './subjects.component.html',
    styleUrls: ['./subjects.component.scss'],
    animations: [routerTransition()]
})
export class SubjectsComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
