import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubjectsRoutingModule } from './subjects-routing.module';
import { SubjectsComponent } from './subjects.component';
import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [CommonModule, SubjectsRoutingModule, PageHeaderModule],
    declarations: [SubjectsComponent]
})
export class SubjectsModule {}
