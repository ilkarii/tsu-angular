import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { SubjectsComponent } from './subjects.component'
import { SubjectsModule } from './subjects.module'

describe('SubjectsComponent', () => {
  let component: SubjectsComponent
  let fixture: ComponentFixture<SubjectsComponent>

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          SubjectsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
         ],
      }).compileComponents()
    })
  )

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
