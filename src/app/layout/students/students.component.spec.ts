import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { StudentsComponent } from './students.component'
import { StudentsModule } from './students.module'

describe('StudentsComponent', () => {
  let component: StudentsComponent
  let fixture: ComponentFixture<StudentsComponent>

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          StudentsModule,
          RouterTestingModule,
          BrowserAnimationsModule,
        ],
      }).compileComponents()
    })
  )

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
