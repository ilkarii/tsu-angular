import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentsRoutingModule } from './students-routing.module';
import { StudentsComponent } from './students.component';
import { PageHeaderModule } from '../../shared';

@NgModule({
    imports: [CommonModule, StudentsRoutingModule, PageHeaderModule],
    declarations: [StudentsComponent]
})
export class StudentsModule {}
